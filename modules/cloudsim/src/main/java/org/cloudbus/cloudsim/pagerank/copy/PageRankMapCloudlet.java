package org.cloudbus.cloudsim.pagerank.copy;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

import org.cloudbus.cloudsim.UtilizationModel;

public class PageRankMapCloudlet extends PageRankCloudlet {

	private String filePath;
	private BufferedReader bufferedReader;

	private Map<String, PageRankIntermediateResult> results;

	public PageRankMapCloudlet(int cloudletId, long cloudletLength, int pesNumber, long cloudletFileSize,
			long cloudletOutputSize, UtilizationModel utilizationModelCpu, UtilizationModel utilizationModelRam,
			UtilizationModel utilizationModelBw) {
		super(cloudletId, cloudletLength, pesNumber, cloudletFileSize, cloudletOutputSize, utilizationModelCpu,
				utilizationModelRam, utilizationModelBw);
	}

	public void initialize(int index, String filePath) {
		super.initialize(index);
		this.filePath = filePath;
		this.results = new TreeMap<String, PageRankIntermediateResult>();

		bufferedReader = PageRankUtils.getBufferedReaderFromFilePath(filePath);
		for (int i = 0; i < this.getIndex() * this.getSize(); i++) {
			try {
				bufferedReader.readLine();
			} catch (IOException e) {
				throw new RuntimeException(PageRankUtils.Messages.FILE_CANNOT_BE_READ.getMessage());
			}
		}
	}

	public void performJob(long mips) {
		if (getCurrentIndex() >= getSize()) {
			return;
		}

		String nodeFrom;
		String nodeTo;
		String line;
		String[] tokens;

		try {
			for (long i = 0; i < Math.min(mips, getSize() - getCurrentIndex()); i++) {
				line = bufferedReader.readLine();

				if (line == null) {
					break;
				}

				tokens = line.split("\\s+");
				nodeFrom = tokens[0];
				nodeTo = tokens[1];

				PageRankIntermediateResult previousResultToUpdateNodes = results.get(nodeTo);
				PageRankIntermediateResult previousResultToUpdateCost = results.get(nodeFrom);
				if (previousResultToUpdateNodes == null) {
					previousResultToUpdateNodes = new PageRankIntermediateResult();
				}
				if (previousResultToUpdateCost == null) {
					previousResultToUpdateCost = new PageRankIntermediateResult();
				}

				previousResultToUpdateNodes.modifyNodes(nodeFrom);
				previousResultToUpdateCost.modifyCost();

				results.put(nodeTo, previousResultToUpdateNodes);
				results.put(nodeFrom, previousResultToUpdateCost);
			}
		} catch (IOException e) {
			throw new RuntimeException(PageRankUtils.Messages.FILE_CANNOT_BE_READ.getMessage());
		}

		this.increaseCurrentIndex(mips);
	}

	public Map<String, PageRankIntermediateResult> getResults() {
		return this.results;
	}

	public String getFilePath() {
		return filePath;
	}

	public String toString() {
		return !this.results.isEmpty() ? this.results.toString() : "not completed yet";
	}

	@Override
	public int getSize() {
		return (int) Math.ceil(
				(double) PageRankCloudlet.getNumberOfVertices() / (double) PageRankCloudlet.getNumberOfCloudlets());
	}

}
