package org.cloudbus.cloudsim.pagerank.copy;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.CloudletSchedulerSpaceShared;
import org.cloudbus.cloudsim.Consts;
import org.cloudbus.cloudsim.ResCloudlet;
import org.cloudbus.cloudsim.core.CloudSim;

public class PageRankCloudletScheduler extends CloudletSchedulerSpaceShared {

	List<Map<String, PageRankIntermediateResult>> intermediateCloudlets = new ArrayList<>();
	CyclicBarrier barrier;

	PageRankCloudletScheduler(CyclicBarrier barrier) {
		this.barrier = barrier;
	}

	@Override
	public double updateVmProcessing(double currentTime, List<Double> mipsShare) {
		System.out.println("updateVmProcessing");
		System.out.println(getCloudletExecList());
		System.out.println(getCloudletWaitingList());
		System.out.println(getCloudletPausedList());
		System.out.println("================");
		setCurrentMipsShare(mipsShare);
		double timeSpam = currentTime - getPreviousTime(); // time since last
															// update
		double capacity = 0.0;
		int cpus = 0;

		for (Double mips : mipsShare) { // count the CPUs available to the VMM
			capacity += mips;
			if (mips > 0) {
				cpus++;
			}
		}
		currentCpus = cpus;
		capacity /= cpus; // average capacity of each cpu

		// each machine in the exec list has the same amount of cpu
		for (ResCloudlet rcl : getCloudletExecList()) {
			rcl.updateCloudletFinishedSoFar((long) (capacity * timeSpam * rcl.getNumberOfPes() * Consts.MILLION));
			((PageRankCloudlet) rcl.getCloudlet()).performJob((int) capacity);
			System.out.println("perform " + capacity * timeSpam * rcl.getNumberOfPes() + " "
					+ ((PageRankCloudlet) rcl.getCloudlet()).getIndex());
			System.out.println("remaining " + rcl.getRemainingCloudletLength());
		}

		// no more cloudlets in this scheduler
		if (getCloudletExecList().size() == 0 && getCloudletWaitingList().size() == 0) {
			setPreviousTime(currentTime);
			return 0.0;
		}

		// update each cloudlet
		int finished = 0;
		List<ResCloudlet> toRemove = new ArrayList<ResCloudlet>();
		for (ResCloudlet rcl : getCloudletExecList()) {
			// finished anyway, rounding issue...
			if (rcl.getRemainingCloudletLength() == 0) {
				toRemove.add(rcl);
				cloudletFinish(rcl);
				finished++;
			}
		}
		getCloudletExecList().removeAll(toRemove);
		System.out.println("removed all from EXEX " + finished + " " + toRemove);

		if (getCloudletWaitingList().isEmpty() && !intermediateCloudlets.isEmpty()) {
			try {
				barrier.await();
			} catch (InterruptedException | BrokenBarrierException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			for (ResCloudlet resCloudlet : getCloudletPausedList()) {
				PageRankCombineCloudlet combineCloudlet = new PageRankCombineCloudlet(
						(PageRankMapCloudlet) resCloudlet.getCloudlet(), intermediateCloudlets);
				ResCloudlet newRcl = new ResCloudlet(combineCloudlet);
				System.out.println("Putting combine back in wait " + combineCloudlet.getIndex());
				newRcl.setCloudletStatus(Cloudlet.QUEUED);
				getCloudletWaitingList().add(newRcl);
				System.out.println("Waiting list " + getCloudletWaitingList());
			}
			getCloudletPausedList().clear();
		}
		
		// for each finished cloudlet, add a new one from the waiting list
		if (!getCloudletWaitingList().isEmpty()) {
			for (int i = 0; i < finished; i++) {
				toRemove.clear();
				for (ResCloudlet rcl : getCloudletWaitingList()) {
					if ((currentCpus - usedPes) >= rcl.getNumberOfPes()) {
						rcl.setCloudletStatus(Cloudlet.INEXEC);
						for (int k = 0; k < rcl.getNumberOfPes(); k++) {
							rcl.setMachineAndPeId(0, i);
						}
						getCloudletExecList().add(rcl);
						usedPes += rcl.getNumberOfPes();
						toRemove.add(rcl);
						break;
					}
				}
				getCloudletWaitingList().removeAll(toRemove);
			}
		}

		// estimate finish time of cloudlets in the execution queue
		double nextEvent = Double.MAX_VALUE;
		for (ResCloudlet rcl : getCloudletExecList()) {
			double remainingLength = rcl.getRemainingCloudletLength();
			double estimatedFinishTime = currentTime + (remainingLength / (capacity * rcl.getNumberOfPes()));
			if (estimatedFinishTime - currentTime < CloudSim.getMinTimeBetweenEvents()) {
				estimatedFinishTime = currentTime + CloudSim.getMinTimeBetweenEvents();
			}
			if (estimatedFinishTime < nextEvent) {
				nextEvent = estimatedFinishTime;
			}
		}
		setPreviousTime(currentTime);
		return nextEvent;
	}

	@Override
	public void cloudletFinish(ResCloudlet rcl) {
		rcl.setCloudletStatus(Cloudlet.SUCCESS);
		rcl.finalizeCloudlet();
		usedPes -= rcl.getNumberOfPes();
		Cloudlet cloudlet = rcl.getCloudlet();
		System.out.println("FINished " + ((PageRankCloudlet) cloudlet).getIndex());
		System.out.println("FINISH QIUEUE IS + " + getCloudletPausedList());
		if (cloudlet instanceof PageRankMapCloudlet) {
			getCloudletPausedList().add(rcl);
			synchronized (intermediateCloudlets) {
				intermediateCloudlets.add(((PageRankMapCloudlet) cloudlet).getResults());
			}
		} else if (cloudlet instanceof PageRankCombineCloudlet) {
			PageRankReduceCloudlet newReduceCloudlet = new PageRankReduceCloudlet((PageRankCombineCloudlet) cloudlet,
					null, 0);
			ResCloudlet newRcl = new ResCloudlet(newReduceCloudlet);
			System.out.println("Putting reduce back in wait " + newReduceCloudlet.getIndex());
			newRcl.setCloudletStatus(Cloudlet.QUEUED);
			getCloudletWaitingList().add(newRcl);
			System.out.println("Waiting list " + getCloudletWaitingList());
		} else {
			getCloudletFinishedList().add(rcl);
		}
	}

}
