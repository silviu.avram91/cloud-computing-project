package org.cloudbus.cloudsim.pagerank;

import java.io.BufferedReader;
import java.util.Map;
import java.util.TreeMap;

import org.cloudbus.cloudsim.UtilizationModel;

public class PageRankMapCloudlet extends PageRankCloudlet {

	private String inputFileName;
	private Map<Integer, PageRankIntermediateResult> results;
	private int nodesCount;
	private int edgesCount;
	private BufferedReader bufferedReader;

	public PageRankMapCloudlet(int cloudletId, long cloudletLength, int pesNumber, long cloudletFileSize,
			long cloudletOutputSize, UtilizationModel utilizationModelCpu, UtilizationModel utilizationModelRam,
			UtilizationModel utilizationModelBw) {
		super(cloudletId, cloudletLength, pesNumber, cloudletFileSize, cloudletOutputSize, utilizationModelCpu,
				utilizationModelRam, utilizationModelBw);
	}

	public void init(String inputFilePath) {
		super.init();

		this.inputFileName = inputFilePath;
		results = new TreeMap<>();

		bufferedReader = PageRankUtils.getBufferedReaderFromFilePath(inputFilePath);

		int[] nodesAndEdges = PageRankUtils.parseLine(bufferedReader);
		nodesCount = nodesAndEdges[0];
		edgesCount = nodesAndEdges[1];

		setCloudletLength(edgesCount);
	}

	@Override
	protected void performJob(int mips) {

		for (int i = 0; i < mips && getCurrentProgress() < edgesCount; i++) {
			int[] nodes = PageRankUtils.parseLine(bufferedReader);

			if (nodes == null) {
				break;
			}

			int inboundNode = nodes[0];
			int outboundNode = nodes[1];

			PageRankIntermediateResult previousResultInboundNode = results.get(inboundNode);
			PageRankIntermediateResult previousResultOutboundNode = results.get(outboundNode);

			if (previousResultInboundNode == null) {
				previousResultInboundNode = new PageRankIntermediateResult();
			}

			if (previousResultOutboundNode == null) {
				previousResultOutboundNode = new PageRankIntermediateResult();
			}

			previousResultInboundNode.modifyCost();
			previousResultOutboundNode.modifyNodes(inboundNode);

			results.put(inboundNode, previousResultInboundNode);
			results.put(outboundNode, previousResultOutboundNode);

			this.incrementCurrentProgress();
		}
	}

	public String getInputFileName() {
		return inputFileName;
	}

	public int getNodesCount() {
		return nodesCount;
	}

	protected Map<Integer, PageRankIntermediateResult> getResults() {
		return results;
	}

	public String toString() {
		return "Map Cloudlet " + getCloudletId() + " of size " + getCloudletLength() + " "
				+ (this.results == null || this.results.isEmpty() ? "not started yet" : this.results.toString());
	}

}
