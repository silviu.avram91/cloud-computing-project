package org.cloudbus.cloudsim.pagerank;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class PageRankUtils {

	public enum Messages {

		FILE_NOT_FOUND("File not found."), FILE_CANNOT_BE_READ("Error when reading the file."), INVALID_REDUCE_PARAMS(
				"Input parameters passed to reduce cloudlet are invalid.");

		private String message;

		private Messages(String message) {
			this.message = message;
		}

		public String getMessage() {
			return this.message;
		}

	}

	public static BufferedReader getBufferedReaderFromFilePath(String filePath) {
		File file;
		FileReader fileReader;
		BufferedReader bufferedReader;

		try {
			file = new File(filePath);
			fileReader = new FileReader(file);
			bufferedReader = new BufferedReader(fileReader);
		} catch (FileNotFoundException e) {
			throw new RuntimeException(Messages.FILE_NOT_FOUND.getMessage());
		}

		return bufferedReader;
	}

	public static int[] parseLine(BufferedReader reader) {
		String[] tokens;
		String line;

		try {
			line = reader.readLine();
		} catch (IOException e) {
			throw new RuntimeException(Messages.FILE_CANNOT_BE_READ.getMessage());
		}

		if (line == null) {
			return null;
		}

		tokens = line.split("\\s+");
		int nodeFrom = Integer.parseInt(tokens[0]);
		int nodeTo = Integer.parseInt(tokens[1]);

		return new int[] { nodeFrom, nodeTo };
	}

	public static Map<Integer, PageRankIntermediateResult> combine(
			List<Map<Integer, PageRankIntermediateResult>> mapResults) {
		Map<Integer, PageRankIntermediateResult> result = new TreeMap<>();

		for (Map<Integer, PageRankIntermediateResult> mapResult : mapResults) {
			for (int node : mapResult.keySet()) {
				PageRankIntermediateResult nodePartialData = mapResult.get(node);
				PageRankIntermediateResult nodeData = result.get(node);

				if (nodeData == null) {
					nodeData = new PageRankIntermediateResult();
				}

				nodeData.modifyCost(nodePartialData.getCost());
				nodeData.modifyNodes(nodePartialData.getNodes());

				result.put(node, nodeData);
			}
		}

		return result;
	}

}
