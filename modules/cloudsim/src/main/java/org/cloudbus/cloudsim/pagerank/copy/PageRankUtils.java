package org.cloudbus.cloudsim.pagerank.copy;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class PageRankUtils {

	public enum Messages {

		FILE_NOT_FOUND("File not found."), FILE_CANNOT_BE_READ("Error when reading the file.");

		private String message;

		private Messages(String message) {
			this.message = message;
		}

		public String getMessage() {
			return this.message;
		}

	}

	public static BufferedReader getBufferedReaderFromFilePath(String filePath) {
		File file;
		FileReader fileReader;
		BufferedReader bufferedReader;

		try {
			file = new File(filePath);
			fileReader = new FileReader(file);
			bufferedReader = new BufferedReader(fileReader);
		} catch (FileNotFoundException e) {
			throw new RuntimeException(Messages.FILE_NOT_FOUND.getMessage());
		}

		return bufferedReader;
	}

}
