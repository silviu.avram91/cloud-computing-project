package org.cloudbus.cloudsim.pagerank.copy;

import java.util.ArrayList;
import java.util.List;

class PageRankIntermediateResult {
	private List<String> nodes;
	private int cost;

	PageRankIntermediateResult() {
		this.nodes = new ArrayList<String>();
		this.cost = 0;
	}

	public PageRankIntermediateResult(List<String> nodes, int cost) {
		this.nodes = nodes;
		this.cost = cost;
	}

	public List<String> getNodes() {
		return nodes;
	}

	public int getCost() {
		return cost;
	}

	public void modifyCost() {
		this.cost++;
	}

	public void modifyCost(int cost) {
		this.cost += cost;
	}

	public void modifyNodes(String node) {
		this.nodes.add(node);
	}

	public void modifyNodes(List<String> nodes) {
		this.nodes.addAll(nodes);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (obj == null || obj.getClass() != this.getClass())
			return false;

		PageRankIntermediateResult otherResult = (PageRankIntermediateResult) obj;
		return otherResult.getCost() == this.cost && this.nodes.equals(otherResult.getNodes());
	}

	public String toString() {
		return cost + " " + this.nodes;
	}

}