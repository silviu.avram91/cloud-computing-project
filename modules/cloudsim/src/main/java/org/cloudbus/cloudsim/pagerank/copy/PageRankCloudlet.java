package org.cloudbus.cloudsim.pagerank.copy;

import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.UtilizationModel;

public abstract class PageRankCloudlet extends Cloudlet {
	private int index;
	private long currentIndex;

	private static int numberOfNodes;
	private static int numberOfVertices;
	private static int numberOfCloudlets;

	public PageRankCloudlet(int cloudletId, long cloudletLength, int pesNumber, long cloudletFileSize,
			long cloudletOutputSize, UtilizationModel utilizationModelCpu, UtilizationModel utilizationModelRam,
			UtilizationModel utilizationModelBw) {
		super(cloudletId, cloudletLength, pesNumber, cloudletFileSize, cloudletOutputSize, utilizationModelCpu,
				utilizationModelRam, utilizationModelBw);
	}

	public void initialize(int index) {
		this.index = index;
		this.currentIndex = 0;
	}

	static public void setGlobals(int numberOfNodes, int numberOfVertices, int numberOfCloudlets) {
		PageRankCloudlet.numberOfNodes = numberOfNodes;
		PageRankCloudlet.numberOfVertices = numberOfVertices;
		PageRankCloudlet.numberOfCloudlets = numberOfCloudlets;
	}

	public abstract void performJob(long mips);

	public int getIndex() {
		return index;
	}

	public abstract int getSize();

	public long getCurrentIndex() {
		return currentIndex;
	}

	public void increaseCurrentIndex(long mips) {
		this.currentIndex += mips;
	}

	public static int getNumberOfNodes() {
		return numberOfNodes;
	}

	public static int getNumberOfVertices() {
		return numberOfVertices;
	}

	public static int getNumberOfCloudlets() {
		return numberOfCloudlets;
	}

}
