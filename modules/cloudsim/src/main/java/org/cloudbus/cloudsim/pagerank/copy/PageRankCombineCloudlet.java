package org.cloudbus.cloudsim.pagerank.copy;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.cloudbus.cloudsim.UtilizationModel;

public class PageRankCombineCloudlet extends PageRankCloudlet {

	List<Map<String, PageRankIntermediateResult>> mapResults;
	Map<String, PageRankIntermediateResult> results;

	public PageRankCombineCloudlet(int cloudletId, long cloudletLength, int pesNumber, long cloudletFileSize,
			long cloudletOutputSize, UtilizationModel utilizationModelCpu, UtilizationModel utilizationModelRam,
			UtilizationModel utilizationModelBw) {
		super(cloudletId, cloudletLength, pesNumber, cloudletFileSize, cloudletOutputSize, utilizationModelCpu,
				utilizationModelRam, utilizationModelBw);
	}

	public PageRankCombineCloudlet(PageRankMapCloudlet mapCloudlet,
			List<Map<String, PageRankIntermediateResult>> mapResults) {
		super(mapCloudlet.getCloudletId(),
				(long) Math.ceil((double) PageRankCloudlet.getNumberOfNodes()
						/ (double) PageRankCloudlet.getNumberOfCloudlets()),
				mapCloudlet.getNumberOfPes(), mapCloudlet.getCloudletFileSize(), mapCloudlet.getCloudletOutputSize(),
				mapCloudlet.getUtilizationModelCpu(), mapCloudlet.getUtilizationModelRam(),
				mapCloudlet.getUtilizationModelBw());
		initialize(mapCloudlet.getIndex(), mapResults);
	}

	public void initialize(int index, List<Map<String, PageRankIntermediateResult>> mapResults) {
		super.initialize(index);
		this.mapResults = mapResults;
		this.results = new TreeMap<String, PageRankIntermediateResult>();
	}

	public void performJob(long mips) {
		for (long i = this.getIndex() * this.getSize() + getCurrentIndex(); i < (this.getIndex() + 1) * this.getSize()
				&& i < this.getIndex() * this.getSize() + getCurrentIndex() + mips; i++) {
			String node = i + "";

			PageRankIntermediateResult previousResult = results.get(node);
			if (previousResult == null) {
				previousResult = new PageRankIntermediateResult();
			}

			for (Map<String, PageRankIntermediateResult> mapResult : mapResults) {
				PageRankIntermediateResult mapResultForNode = mapResult.get(node);
				if (mapResultForNode == null) {
					continue;
				}

				previousResult.modifyCost(mapResultForNode.getCost());
				previousResult.modifyNodes(mapResultForNode.getNodes());

				results.put(node, previousResult);
			}

		}

		this.increaseCurrentIndex(mips);
	}

	public Map<String, PageRankIntermediateResult> getResults() {
		return results;
	}

	@Override
	public int getSize() {
		return (int) Math
				.ceil((double) PageRankCloudlet.getNumberOfNodes() / (double) PageRankCloudlet.getNumberOfCloudlets());
	}
}
