package org.cloudbus.cloudsim.pagerank;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.cloudbus.cloudsim.UtilizationModel;

public class PageRankReduceCloudlet extends PageRankCloudlet {

	private Map<Integer, PageRankIntermediateResult> inputMapResults;
	private Map<Integer, Double> initialPageRanks;
	private Map<Integer, Double> results;
	private double dampingFactor;
	private int size;
	private int start;

	public PageRankReduceCloudlet(int cloudletId, long cloudletLength, int pesNumber, long cloudletFileSize,
			long cloudletOutputSize, UtilizationModel utilizationModelCpu, UtilizationModel utilizationModelRam,
			UtilizationModel utilizationModelBw) {
		super(cloudletId, cloudletLength, pesNumber, cloudletFileSize, cloudletOutputSize, utilizationModelCpu,
				utilizationModelRam, utilizationModelBw);
	}

	public void init(int start, int size, Map<Integer, PageRankIntermediateResult> inputMapResults,
			Map<Integer, Double> initialPageRanks, double dampingFactor) {
		if (initialPageRanks == null || inputMapResults == null || dampingFactor < 0.0 || dampingFactor > 1.0
				|| start < 0 || size < 0) {
			throw new RuntimeException(PageRankUtils.Messages.INVALID_REDUCE_PARAMS.getMessage());
		}
		super.init();

		this.initialPageRanks = initialPageRanks;
		this.inputMapResults = inputMapResults;
		this.dampingFactor = dampingFactor;
		this.start = start;
		this.size = Math.min(size, initialPageRanks.size() - start);
		setCloudletLength(this.size);

		results = new TreeMap<>();
	}

	@Override
	protected void performJob(int mips) {
		for (int i = 0; i < mips && getCurrentProgress() < size; i++) {
			int nodeIndex = start + getCurrentProgress();

			List<Integer> inboundNodes = inputMapResults.get(nodeIndex).getNodes();
			double pageRank = 1 - dampingFactor
					+ dampingFactor * inboundNodes.stream()
							.map(n -> (double) initialPageRanks.get(n) / (double) inputMapResults.get(n).getCost())
							.mapToDouble(Double::doubleValue).sum();
			results.put(nodeIndex, pageRank);

			incrementCurrentProgress();
		}
	}

	protected Map<Integer, Double> getResults() {
		return results;
	}

	public String toString() {
		return "Reduce Cloudlet " + getCloudletId() + " of size " + getCloudletLength() + " "
				+ (this.results == null || this.results.isEmpty() ? "not started yet" : this.results.toString());
	}

}
