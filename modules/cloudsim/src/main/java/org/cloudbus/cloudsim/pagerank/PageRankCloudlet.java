package org.cloudbus.cloudsim.pagerank;

import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.UtilizationModel;

public abstract class PageRankCloudlet extends Cloudlet {

	private int currentProgress;

	public PageRankCloudlet(int cloudletId, long cloudletLength, int pesNumber, long cloudletFileSize,
			long cloudletOutputSize, UtilizationModel utilizationModelCpu, UtilizationModel utilizationModelRam,
			UtilizationModel utilizationModelBw) {
		super(cloudletId, cloudletLength, pesNumber, cloudletFileSize, cloudletOutputSize, utilizationModelCpu,
				utilizationModelRam, utilizationModelBw);
	}

	protected void init() {
		this.currentProgress = 0;
	}

	protected abstract void performJob(int mips);

	protected int getCurrentProgress() {
		return currentProgress;
	}

	protected void incrementCurrentProgress() {
		currentProgress++;
	}

}
