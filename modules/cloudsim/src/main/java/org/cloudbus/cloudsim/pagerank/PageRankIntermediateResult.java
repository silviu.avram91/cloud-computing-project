package org.cloudbus.cloudsim.pagerank;

import java.util.ArrayList;
import java.util.List;

public class PageRankIntermediateResult {
	private List<Integer> nodes;
	private int cost;

	PageRankIntermediateResult() {
		this.nodes = new ArrayList<Integer>();
		this.cost = 0;
	}

	public PageRankIntermediateResult(List<Integer> nodes, int cost) {
		this.nodes = nodes;
		this.cost = cost;
	}

	public List<Integer> getNodes() {
		return nodes;
	}

	public int getCost() {
		return cost;
	}

	public void modifyCost() {
		this.cost++;
	}

	public void modifyCost(int cost) {
		this.cost += cost;
	}

	public void modifyNodes(int node) {
		this.nodes.add(node);
	}

	public void modifyNodes(List<Integer> nodes) {
		this.nodes.addAll(nodes);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (obj == null || obj.getClass() != this.getClass())
			return false;

		PageRankIntermediateResult otherResult = (PageRankIntermediateResult) obj;
		return otherResult.getCost() == this.cost && this.nodes.equals(otherResult.getNodes());
	}

	public String toString() {
		return cost + " " + this.nodes;
	}

}