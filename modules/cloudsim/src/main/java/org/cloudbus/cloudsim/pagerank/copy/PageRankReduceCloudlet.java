package org.cloudbus.cloudsim.pagerank.copy;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.cloudbus.cloudsim.UtilizationModel;

public class PageRankReduceCloudlet extends PageRankCloudlet {

	private Map<String, PageRankIntermediateResult> combineResult;
	private Map<String, Double> initialPageRanks;
	private Map<String, Double> results;
	private double dampingFactor;

	public PageRankReduceCloudlet(int cloudletId, long cloudletLength, int pesNumber, long cloudletFileSize,
			long cloudletOutputSize, UtilizationModel utilizationModelCpu, UtilizationModel utilizationModelRam,
			UtilizationModel utilizationModelBw) {
		super(cloudletId, cloudletLength, pesNumber, cloudletFileSize, cloudletOutputSize, utilizationModelCpu,
				utilizationModelRam, utilizationModelBw);
	}

	public PageRankReduceCloudlet(PageRankCombineCloudlet combineCloudlet, Map<String, Double> initialPageRanks,
			double dampingFactor) {
		super(combineCloudlet.getCloudletId(), combineCloudlet.getCloudletLength(), combineCloudlet.getNumberOfPes(),
				combineCloudlet.getCloudletFileSize(), combineCloudlet.getCloudletOutputSize(),
				combineCloudlet.getUtilizationModelCpu(), combineCloudlet.getUtilizationModelRam(),
				combineCloudlet.getUtilizationModelBw());
		initialize(combineCloudlet.getIndex(), combineCloudlet.getResults(), initialPageRanks, dampingFactor);
	}

	public void initialize(int index, Map<String, PageRankIntermediateResult> combineResult,
			Map<String, Double> initialPageRanks, double dampingFactor) {
		super.initialize(index);
		this.combineResult = combineResult;
		this.initialPageRanks = initialPageRanks;
		this.results = new TreeMap<String, Double>();
		this.dampingFactor = dampingFactor;
	}

	public void performJob(long mips) {
		for (long i = this.getIndex() * this.getSize() + getCurrentIndex(); i < (this.getIndex() + 1) * this.getSize()
				&& i < this.getIndex() * this.getSize() + getCurrentIndex() + mips; i++) {
			String node = i + "";
			List<String> nodes = combineResult.get(node).getNodes();
			double pageRank = 1 - dampingFactor
					+ dampingFactor
							* nodes.stream().map((n) -> initialPageRanks.get(n) / combineResult.get(n).getCost())
									.mapToDouble(Double::doubleValue).sum();
			results.put(node, pageRank);
		}

		this.increaseCurrentIndex(mips);
	}

	public Map<String, Double> getResults() {
		return results;
	}

	@Override
	public int getSize() {
		return (int) Math
				.ceil((double) PageRankCloudlet.getNumberOfNodes() / (double) PageRankCloudlet.getNumberOfCloudlets());
	}

}
