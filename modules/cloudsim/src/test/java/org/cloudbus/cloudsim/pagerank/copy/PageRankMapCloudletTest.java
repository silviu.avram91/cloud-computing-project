package org.cloudbus.cloudsim.pagerank.copy;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;

import org.cloudbus.cloudsim.UtilizationModel;
import org.cloudbus.cloudsim.UtilizationModelFull;
import org.cloudbus.cloudsim.pagerank.copy.PageRankIntermediateResult;
import org.cloudbus.cloudsim.pagerank.copy.PageRankMapCloudlet;
import org.junit.Before;
import org.junit.Test;

public class PageRankMapCloudletTest {

	PageRankMapCloudlet cloudlet;

	@Before
	public void setUp() {
		UtilizationModel utilizationModel = new UtilizationModelFull();

		cloudlet = new PageRankMapCloudlet(0, 400000, 1, 300, 300, utilizationModel, utilizationModel,
				utilizationModel);
	}

	@Test
	public void testValidInputParams() {
		String filePath = "src/test/resources/testRunMapCloudlet1";
		int index = 0;
		int size = 4;
		cloudlet.initialize(index, filePath);
		PageRankCloudlet.setGlobals(4, 7, 2);

		assertEquals(index, cloudlet.getIndex());
		assertEquals(size, cloudlet.getSize());
		assertEquals(filePath, cloudlet.getFilePath());
	}

	@Test
	public void testRunWithFullSize() {
		String filePath = "src/test/resources/testRunMapCloudlet1";
		int index = 0;
		int mips = 7;
		PageRankCloudlet.setGlobals(4, 7, 1);

		cloudlet.initialize(index, filePath);
		cloudlet.performJob(mips);

		Map<String, PageRankIntermediateResult> expected = new TreeMap<String, PageRankIntermediateResult>();
		expected.put("0", new PageRankIntermediateResult(Arrays.asList("2"), 3));
		expected.put("1", new PageRankIntermediateResult(Arrays.asList("0", "2"), 1));
		expected.put("2", new PageRankIntermediateResult(Arrays.asList("0", "1", "3"), 2));
		expected.put("3", new PageRankIntermediateResult(Arrays.asList("0"), 1));

		assertEquals(expected, cloudlet.getResults());
	}

	@Test
	public void testRunWithPartialSize() {
		String filePath = "src/test/resources/testRunMapCloudlet1";
		int index = 0;
		PageRankCloudlet.setGlobals(4, 7, 3);
		int mips = 7;
		cloudlet.initialize(index, filePath);
		cloudlet.performJob(mips);

		Map<String, PageRankIntermediateResult> expected = new TreeMap<String, PageRankIntermediateResult>();
		expected.put("0", new PageRankIntermediateResult(Arrays.asList(), 3));
		expected.put("1", new PageRankIntermediateResult(Arrays.asList("0"), 0));
		expected.put("2", new PageRankIntermediateResult(Arrays.asList("0"), 0));
		expected.put("3", new PageRankIntermediateResult(Arrays.asList("0"), 0));

		assertEquals(expected, cloudlet.getResults());
	}

	@Test
	public void testRunWithHigherOffset() {
		String filePath = "src/test/resources/testRunMapCloudlet1";
		int index = 1;
		PageRankCloudlet.setGlobals(4, 7, 4);
		int mips = 7;
		cloudlet.initialize(index, filePath);
		cloudlet.performJob(mips);

		Map<String, PageRankIntermediateResult> expected = new TreeMap<String, PageRankIntermediateResult>();
		expected.put("0", new PageRankIntermediateResult(Arrays.asList(), 1));
		expected.put("1", new PageRankIntermediateResult(Arrays.asList(), 1));
		expected.put("2", new PageRankIntermediateResult(Arrays.asList("1"), 0));
		expected.put("3", new PageRankIntermediateResult(Arrays.asList("0"), 0));

		assertEquals(expected, cloudlet.getResults());
	}

	@Test
	public void testMultipleRuns() {
		String filePath = "src/test/resources/testRunMapCloudlet1";
		int index = 0;
		PageRankCloudlet.setGlobals(4, 7, 4);
		int mips = 7;
		cloudlet.initialize(index, filePath);
		cloudlet.performJob(mips);

		Map<String, PageRankIntermediateResult> expected = new TreeMap<String, PageRankIntermediateResult>();
		expected.put("0", new PageRankIntermediateResult(Arrays.asList(), 2));
		expected.put("1", new PageRankIntermediateResult(Arrays.asList("0"), 0));
		expected.put("2", new PageRankIntermediateResult(Arrays.asList("0"), 0));

		assertEquals(expected, cloudlet.getResults());

		cloudlet.initialize(index + 1, filePath);
		cloudlet.performJob(mips);

		expected.clear();
		expected.put("0", new PageRankIntermediateResult(Arrays.asList(), 1));
		expected.put("1", new PageRankIntermediateResult(Arrays.asList(), 1));
		expected.put("2", new PageRankIntermediateResult(Arrays.asList("1"), 0));
		expected.put("3", new PageRankIntermediateResult(Arrays.asList("0"), 0));

		assertEquals(expected, cloudlet.getResults());

		cloudlet.initialize(index + 2, filePath);
		cloudlet.performJob(mips);

		expected.clear();
		expected.put("0", new PageRankIntermediateResult(Arrays.asList("2"), 0));
		expected.put("1", new PageRankIntermediateResult(Arrays.asList("2"), 0));
		expected.put("2", new PageRankIntermediateResult(Arrays.asList(), 2));

		assertEquals(expected, cloudlet.getResults());

		cloudlet.initialize(index + 3, filePath);
		cloudlet.performJob(mips);

		expected.clear();
		expected.put("2", new PageRankIntermediateResult(Arrays.asList("3"), 0));
		expected.put("3", new PageRankIntermediateResult(Arrays.asList(), 1));

		assertEquals(expected, cloudlet.getResults());
	}

	@Test
	public void testMultipleRunsWithPartialMIPS() {
		String filePath = "src/test/resources/testRunMapCloudlet1";
		int index = 0;
		PageRankCloudlet.setGlobals(4, 7, 4);
		int mips = 1;
		cloudlet.initialize(index, filePath);
		cloudlet.performJob(mips);

		Map<String, PageRankIntermediateResult> expected = new TreeMap<String, PageRankIntermediateResult>();
		expected.put("0", new PageRankIntermediateResult(Arrays.asList(), 1));
		expected.put("1", new PageRankIntermediateResult(Arrays.asList("0"), 0));

		assertEquals(expected, cloudlet.getResults());

		cloudlet.performJob(0); // for 0 mips, result remains the same.

		assertEquals(expected, cloudlet.getResults());

		cloudlet.performJob(mips);

		expected.clear();
		expected.put("0", new PageRankIntermediateResult(Arrays.asList(), 2));
		expected.put("1", new PageRankIntermediateResult(Arrays.asList("0"), 0));
		expected.put("2", new PageRankIntermediateResult(Arrays.asList("0"), 0));

		assertEquals(expected, cloudlet.getResults());

		cloudlet.initialize(index + 1, filePath);
		cloudlet.performJob(mips);

		expected.clear();
		expected.put("0", new PageRankIntermediateResult(Arrays.asList(), 1));
		expected.put("3", new PageRankIntermediateResult(Arrays.asList("0"), 0));

		assertEquals(expected, cloudlet.getResults());

		cloudlet.performJob(mips);

		expected.clear();
		expected.put("0", new PageRankIntermediateResult(Arrays.asList(), 1));
		expected.put("1", new PageRankIntermediateResult(Arrays.asList(), 1));
		expected.put("2", new PageRankIntermediateResult(Arrays.asList("1"), 0));
		expected.put("3", new PageRankIntermediateResult(Arrays.asList("0"), 0));

		assertEquals(expected, cloudlet.getResults());

		cloudlet.initialize(index + 2, filePath);
		cloudlet.performJob(mips);

		expected.clear();
		expected.put("0", new PageRankIntermediateResult(Arrays.asList("2"), 0));
		expected.put("2", new PageRankIntermediateResult(Arrays.asList(), 1));

		assertEquals(expected, cloudlet.getResults());

		cloudlet.performJob(mips);

		expected.clear();
		expected.put("0", new PageRankIntermediateResult(Arrays.asList("2"), 0));
		expected.put("1", new PageRankIntermediateResult(Arrays.asList("2"), 0));
		expected.put("2", new PageRankIntermediateResult(Arrays.asList(), 2));

		assertEquals(expected, cloudlet.getResults());

		cloudlet.initialize(index + 3, filePath);
		cloudlet.performJob(mips);

		expected.clear();
		expected.put("2", new PageRankIntermediateResult(Arrays.asList("3"), 0));
		expected.put("3", new PageRankIntermediateResult(Arrays.asList(), 1));

		assertEquals(expected, cloudlet.getResults());

		cloudlet.performJob(mips);

		expected.clear();
		expected.put("2", new PageRankIntermediateResult(Arrays.asList("3"), 0));
		expected.put("3", new PageRankIntermediateResult(Arrays.asList(), 1));

		assertEquals(expected, cloudlet.getResults());
	}
}
