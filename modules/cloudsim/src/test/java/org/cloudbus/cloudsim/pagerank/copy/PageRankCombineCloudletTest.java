package org.cloudbus.cloudsim.pagerank.copy;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;

import org.cloudbus.cloudsim.UtilizationModel;
import org.cloudbus.cloudsim.UtilizationModelFull;
import org.cloudbus.cloudsim.pagerank.copy.PageRankCombineCloudlet;
import org.cloudbus.cloudsim.pagerank.copy.PageRankIntermediateResult;
import org.junit.Before;
import org.junit.Test;

public class PageRankCombineCloudletTest {

	PageRankCombineCloudlet cloudlet;

	@Before
	public void setUp() {
		UtilizationModel utilizationModel = new UtilizationModelFull();

		cloudlet = new PageRankCombineCloudlet(0, 400000, 1, 300, 300, utilizationModel, utilizationModel,
				utilizationModel);
	}

	@Test
	public void testValidInputParams() {
		int index = 0;
		int size = 2;
		cloudlet.initialize(index);
		PageRankCloudlet.setGlobals(4, 7, 2);

		assertEquals(index, cloudlet.getIndex());
		assertEquals(size, cloudlet.getSize());
	}

	@Test
	public void testRunWithFullSize() {
		int index = 0;
		PageRankCloudlet.setGlobals(4, 7, 1);
		int mips = 4;

		Map<String, PageRankIntermediateResult> inputs = new TreeMap<String, PageRankIntermediateResult>();
		inputs.put("0", new PageRankIntermediateResult(Arrays.asList("2"), 3));
		inputs.put("1", new PageRankIntermediateResult(Arrays.asList("0", "2"), 1));
		inputs.put("2", new PageRankIntermediateResult(Arrays.asList("0", "1", "3"), 2));
		inputs.put("3", new PageRankIntermediateResult(Arrays.asList("0"), 1));

		cloudlet.initialize(index, Arrays.asList(inputs));
		cloudlet.performJob(mips);

		assertEquals(inputs, cloudlet.getResults());
	}

	@Test
	public void testRunWithPartialSizes() {
		int index = 0;
		int mips = 2;
		PageRankCloudlet.setGlobals(4, 7, 2);

		Map<String, PageRankIntermediateResult> inputs = new TreeMap<String, PageRankIntermediateResult>();
		inputs.put("0", new PageRankIntermediateResult(Arrays.asList("2"), 3));
		inputs.put("1", new PageRankIntermediateResult(Arrays.asList("0", "2"), 1));
		inputs.put("2", new PageRankIntermediateResult(Arrays.asList("0", "1", "3"), 2));
		inputs.put("3", new PageRankIntermediateResult(Arrays.asList("0"), 1));

		cloudlet.initialize(index, Arrays.asList(inputs));
		cloudlet.performJob(mips);

		Map<String, PageRankIntermediateResult> expected = new TreeMap<String, PageRankIntermediateResult>();
		expected.put("0", new PageRankIntermediateResult(Arrays.asList("2"), 3));
		expected.put("1", new PageRankIntermediateResult(Arrays.asList("0", "2"), 1));

		assertEquals(expected, cloudlet.getResults());

		expected.clear();
		expected.put("2", new PageRankIntermediateResult(Arrays.asList("0", "1", "3"), 2));
		expected.put("3", new PageRankIntermediateResult(Arrays.asList("0"), 1));

		cloudlet.initialize(index + 1, Arrays.asList(inputs));
		cloudlet.performJob(mips);

		assertEquals(expected, cloudlet.getResults());
	}

	@Test
	public void testRunWithPartiallySplitInputsAndFullSize() {
		int index = 0;
		int mips = 4;
		PageRankCloudlet.setGlobals(4, 7, 1);

		Map<String, PageRankIntermediateResult> inputs1 = new TreeMap<String, PageRankIntermediateResult>();
		inputs1.put("0", new PageRankIntermediateResult(Arrays.asList(), 2));
		inputs1.put("1", new PageRankIntermediateResult(Arrays.asList("0"), 0));
		inputs1.put("2", new PageRankIntermediateResult(Arrays.asList("0"), 0));

		Map<String, PageRankIntermediateResult> inputs2 = new TreeMap<String, PageRankIntermediateResult>();
		inputs2.put("0", new PageRankIntermediateResult(Arrays.asList(), 1));
		inputs2.put("1", new PageRankIntermediateResult(Arrays.asList(), 1));
		inputs2.put("2", new PageRankIntermediateResult(Arrays.asList("1"), 0));
		inputs2.put("3", new PageRankIntermediateResult(Arrays.asList("0"), 0));

		Map<String, PageRankIntermediateResult> inputs3 = new TreeMap<String, PageRankIntermediateResult>();
		inputs3.put("0", new PageRankIntermediateResult(Arrays.asList("2"), 0));
		inputs3.put("1", new PageRankIntermediateResult(Arrays.asList("2"), 0));
		inputs3.put("2", new PageRankIntermediateResult(Arrays.asList(), 2));

		Map<String, PageRankIntermediateResult> inputs4 = new TreeMap<String, PageRankIntermediateResult>();
		inputs4.put("2", new PageRankIntermediateResult(Arrays.asList("3"), 0));
		inputs4.put("3", new PageRankIntermediateResult(Arrays.asList(), 1));

		Map<String, PageRankIntermediateResult> expected = new TreeMap<String, PageRankIntermediateResult>();
		expected.put("0", new PageRankIntermediateResult(Arrays.asList("2"), 3));
		expected.put("1", new PageRankIntermediateResult(Arrays.asList("0", "2"), 1));
		expected.put("2", new PageRankIntermediateResult(Arrays.asList("0", "1", "3"), 2));
		expected.put("3", new PageRankIntermediateResult(Arrays.asList("0"), 1));

		cloudlet.initialize(index, Arrays.asList(inputs1, inputs2, inputs3, inputs4));
		cloudlet.performJob(mips);

		assertEquals(expected, cloudlet.getResults());
	}

	@Test
	public void testRunWithPartiallySplitInputsAndPartialSize() {
		int index = 0;
		int mips = 2;
		PageRankCloudlet.setGlobals(4, 7, 2);

		Map<String, PageRankIntermediateResult> inputs1 = new TreeMap<String, PageRankIntermediateResult>();
		inputs1.put("0", new PageRankIntermediateResult(Arrays.asList(), 2));
		inputs1.put("1", new PageRankIntermediateResult(Arrays.asList("0"), 0));
		inputs1.put("2", new PageRankIntermediateResult(Arrays.asList("0"), 0));

		Map<String, PageRankIntermediateResult> inputs2 = new TreeMap<String, PageRankIntermediateResult>();
		inputs2.put("0", new PageRankIntermediateResult(Arrays.asList(), 1));
		inputs2.put("1", new PageRankIntermediateResult(Arrays.asList(), 1));
		inputs2.put("2", new PageRankIntermediateResult(Arrays.asList("1"), 0));
		inputs2.put("3", new PageRankIntermediateResult(Arrays.asList("0"), 0));

		Map<String, PageRankIntermediateResult> inputs3 = new TreeMap<String, PageRankIntermediateResult>();
		inputs3.put("0", new PageRankIntermediateResult(Arrays.asList("2"), 0));
		inputs3.put("1", new PageRankIntermediateResult(Arrays.asList("2"), 0));
		inputs3.put("2", new PageRankIntermediateResult(Arrays.asList(), 2));

		Map<String, PageRankIntermediateResult> inputs4 = new TreeMap<String, PageRankIntermediateResult>();
		inputs4.put("2", new PageRankIntermediateResult(Arrays.asList("3"), 0));
		inputs4.put("3", new PageRankIntermediateResult(Arrays.asList(), 1));

		Map<String, PageRankIntermediateResult> expected = new TreeMap<String, PageRankIntermediateResult>();
		expected.put("0", new PageRankIntermediateResult(Arrays.asList("2"), 3));
		expected.put("1", new PageRankIntermediateResult(Arrays.asList("0", "2"), 1));

		cloudlet.initialize(index, Arrays.asList(inputs1, inputs2, inputs3, inputs4));
		cloudlet.performJob(mips);

		assertEquals(expected, cloudlet.getResults());

		expected.clear();
		expected.put("2", new PageRankIntermediateResult(Arrays.asList("0", "1", "3"), 2));
		expected.put("3", new PageRankIntermediateResult(Arrays.asList("0"), 1));

		cloudlet.initialize(index + 1, Arrays.asList(inputs1, inputs2, inputs3, inputs4));
		cloudlet.performJob(mips);

		assertEquals(expected, cloudlet.getResults());
	}

	@Test
	public void testRunWithPartiallySplitInputsAndPartialSizeAndPartialMIPS() {
		int index = 0;
		int mips = 1;
		PageRankCloudlet.setGlobals(4, 7, 2);

		Map<String, PageRankIntermediateResult> inputs1 = new TreeMap<String, PageRankIntermediateResult>();
		inputs1.put("0", new PageRankIntermediateResult(Arrays.asList(), 2));
		inputs1.put("1", new PageRankIntermediateResult(Arrays.asList("0"), 0));
		inputs1.put("2", new PageRankIntermediateResult(Arrays.asList("0"), 0));

		Map<String, PageRankIntermediateResult> inputs2 = new TreeMap<String, PageRankIntermediateResult>();
		inputs2.put("0", new PageRankIntermediateResult(Arrays.asList(), 1));
		inputs2.put("1", new PageRankIntermediateResult(Arrays.asList(), 1));
		inputs2.put("2", new PageRankIntermediateResult(Arrays.asList("1"), 0));
		inputs2.put("3", new PageRankIntermediateResult(Arrays.asList("0"), 0));

		Map<String, PageRankIntermediateResult> inputs3 = new TreeMap<String, PageRankIntermediateResult>();
		inputs3.put("0", new PageRankIntermediateResult(Arrays.asList("2"), 0));
		inputs3.put("1", new PageRankIntermediateResult(Arrays.asList("2"), 0));
		inputs3.put("2", new PageRankIntermediateResult(Arrays.asList(), 2));

		Map<String, PageRankIntermediateResult> inputs4 = new TreeMap<String, PageRankIntermediateResult>();
		inputs4.put("2", new PageRankIntermediateResult(Arrays.asList("3"), 0));
		inputs4.put("3", new PageRankIntermediateResult(Arrays.asList(), 1));

		Map<String, PageRankIntermediateResult> expected = new TreeMap<String, PageRankIntermediateResult>();
		expected.put("0", new PageRankIntermediateResult(Arrays.asList("2"), 3));

		cloudlet.initialize(index, Arrays.asList(inputs1, inputs2, inputs3, inputs4));
		cloudlet.performJob(mips);

		assertEquals(expected, cloudlet.getResults());

		cloudlet.performJob(0);

		assertEquals(expected, cloudlet.getResults());

		cloudlet.performJob(mips);

		expected.put("1", new PageRankIntermediateResult(Arrays.asList("0", "2"), 1));

		assertEquals(expected, cloudlet.getResults());

		expected.clear();
		expected.put("2", new PageRankIntermediateResult(Arrays.asList("0", "1", "3"), 2));

		cloudlet.initialize(index + 1, Arrays.asList(inputs1, inputs2, inputs3, inputs4));
		cloudlet.performJob(mips);

		assertEquals(expected, cloudlet.getResults());

		expected.put("3", new PageRankIntermediateResult(Arrays.asList("0"), 1));
		cloudlet.performJob(mips);

		assertEquals(expected, cloudlet.getResults());
	}

}
