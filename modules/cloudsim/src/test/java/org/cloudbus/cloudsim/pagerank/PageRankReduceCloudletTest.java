package org.cloudbus.cloudsim.pagerank;

import org.cloudbus.cloudsim.UtilizationModel;
import org.cloudbus.cloudsim.UtilizationModelFull;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;

public class PageRankReduceCloudletTest {
	PageRankReduceCloudlet cloudlet;
	static Map<Integer, Double> initialPageRanks;
	static int totalNodes;
	static double dampingFactor;
	static Map<Integer, PageRankIntermediateResult> inputMapResults;

	@BeforeClass
	public static void setUpForAll() {
		initialPageRanks = new TreeMap<>();
		totalNodes = 7;
		dampingFactor = 0.85;

		inputMapResults = new TreeMap<Integer, PageRankIntermediateResult>();

		inputMapResults.put(0, new PageRankIntermediateResult(Arrays.asList(2, 4, 6), 3));
		inputMapResults.put(1, new PageRankIntermediateResult(Arrays.asList(0, 2, 6), 1));
		inputMapResults.put(2, new PageRankIntermediateResult(Arrays.asList(0, 1, 3, 4, 6), 2));
		inputMapResults.put(3, new PageRankIntermediateResult(Arrays.asList(0, 5), 1));
		inputMapResults.put(4, new PageRankIntermediateResult(Arrays.asList(5, 6), 2));
		inputMapResults.put(5, new PageRankIntermediateResult(Arrays.asList(), 2));
		inputMapResults.put(6, new PageRankIntermediateResult(Arrays.asList(), 4));

		for (int i = 0; i < totalNodes; i++) {
			initialPageRanks.put(i, 1.0);
		}
	}

	@Before
	public void setUp() {
		UtilizationModel utilizationModel = new UtilizationModelFull();

		cloudlet = new PageRankReduceCloudlet(0, 400000, 1, 300, 300, utilizationModel, utilizationModel,
				utilizationModel);
	}

	@Test
	public void testInvalidParams() {
		try {
			cloudlet.init(1, 1, null, new TreeMap<>(), 0.3);
		} catch (RuntimeException e) {
			assertEquals(e.getMessage(), PageRankUtils.Messages.INVALID_REDUCE_PARAMS.getMessage());
		}
		try {
			cloudlet.init(1, 1, new TreeMap<>(), null, 0.3);
		} catch (RuntimeException e) {
			assertEquals(e.getMessage(), PageRankUtils.Messages.INVALID_REDUCE_PARAMS.getMessage());
		}
		try {
			cloudlet.init(1, 1, new TreeMap<>(), new TreeMap<>(), -1.0);
		} catch (RuntimeException e) {
			assertEquals(e.getMessage(), PageRankUtils.Messages.INVALID_REDUCE_PARAMS.getMessage());
		}
		try {
			cloudlet.init(1, 1, null, null, 0.3);
		} catch (RuntimeException e) {
			assertEquals(e.getMessage(), PageRankUtils.Messages.INVALID_REDUCE_PARAMS.getMessage());
		}
		try {
			cloudlet.init(1, 1, null, null, 1.1);
		} catch (RuntimeException e) {
			assertEquals(e.getMessage(), PageRankUtils.Messages.INVALID_REDUCE_PARAMS.getMessage());
		}
		try {
			cloudlet.init(-1, 1, new TreeMap<>(), new TreeMap<>(), 0.5);
		} catch (RuntimeException e) {
			assertEquals(e.getMessage(), PageRankUtils.Messages.INVALID_REDUCE_PARAMS.getMessage());
		}
		try {
			cloudlet.init(0, -1, new TreeMap<>(), new TreeMap<>(), 0.5);
		} catch (RuntimeException e) {
			assertEquals(e.getMessage(), PageRankUtils.Messages.INVALID_REDUCE_PARAMS.getMessage());
		}
		try {
			cloudlet.init(-1, -1, new TreeMap<>(), new TreeMap<>(), 0.5);
		} catch (RuntimeException e) {
			assertEquals(e.getMessage(), PageRankUtils.Messages.INVALID_REDUCE_PARAMS.getMessage());
		}
	}

	@Test
	public void testValidParams() {
		try {
			cloudlet.init(1, 2, new TreeMap<>(), new TreeMap<>(), 0.5);
		} catch (RuntimeException e) {
			throw new AssertionError("failed initialization");
		}
	}

	@Test
	public void testRunWithExactMips() {
		int start = 0;
		int size = 7;
		int mips = totalNodes;
		Map<Integer, Double> result;

		cloudlet.init(start, size, inputMapResults, initialPageRanks, dampingFactor);
		cloudlet.performJob(mips);
		result = cloudlet.getResults();
		int i = 0;

		assertEquals(size, result.size());
		for (Double expectedPageRank : new Double[] { 1.212, 1.070, 2.770, 0.858, 0.787, 0.150, 0.150 }) {
			assertEquals(expectedPageRank, result.get(i++), 0.001);
		}
	}

	@Test
	public void testRunWithExactMipsGreaterInputSize() {
		int start = 0;
		int size = 7;
		int mips = totalNodes;
		Map<Integer, Double> result;

		cloudlet.init(start, size + 20, inputMapResults, initialPageRanks, dampingFactor);
		cloudlet.performJob(mips);
		result = cloudlet.getResults();
		int i = 0;

		assertEquals(size, result.size());
		for (Double expectedPageRank : new Double[] { 1.212, 1.070, 2.770, 0.858, 0.787, 0.150, 0.150 }) {
			assertEquals(expectedPageRank, result.get(i++), 0.001);
		}
	}

	@Test
	public void testRunWithExactMipsLesserInputSize() {
		int start = 0;
		int size = 3;
		int mips = totalNodes;
		Map<Integer, Double> result;

		cloudlet.init(start, size, inputMapResults, initialPageRanks, dampingFactor);
		cloudlet.performJob(mips);
		result = cloudlet.getResults();
		int i = 0;

		assertEquals(size, result.size());
		for (Double expectedPageRank : new Double[] { 1.212, 1.070, 2.770 }) {
			assertEquals(expectedPageRank, result.get(i++), 0.001);
		}
	}

	@Test
	public void testRunWithMoreMips() {
		int start = 0;
		int size = 7;
		int mips = totalNodes * 2;
		Map<Integer, Double> result;

		cloudlet.init(start, size, inputMapResults, initialPageRanks, dampingFactor);
		cloudlet.performJob(mips);
		result = cloudlet.getResults();
		int i = 0;

		assertEquals(size, result.size());
		for (Double expectedPageRank : new Double[] { 1.212, 1.070, 2.770, 0.858, 0.787, 0.150, 0.150 }) {
			assertEquals(expectedPageRank, result.get(i++), 0.001);
		}
	}

	@Test
	public void testRunWithFewerMips() {
		int start = 0;
		int size = 7;
		int mips = 2;
		Map<Integer, Double> result;

		cloudlet.init(start, size, inputMapResults, initialPageRanks, dampingFactor);
		cloudlet.performJob(mips);
		result = cloudlet.getResults();
		int i = 0;

		assertEquals(mips, result.size());
		for (Double expectedPageRank : new Double[] { 1.212, 1.070 }) {
			assertEquals(expectedPageRank, result.get(i++), 0.001);
		}
	}

	@Test
	public void testRunWithExactMipsTwoCloudlets() {
		int start = 0;
		int size = 4;
		int mips = 4;
		Map<Integer, Double> result;

		cloudlet.init(start, size, inputMapResults, initialPageRanks, dampingFactor);
		cloudlet.performJob(mips);
		result = cloudlet.getResults();
		int i = 0;

		assertEquals(size, result.size());
		for (Double expectedPageRank : new Double[] { 1.212, 1.070, 2.770, 0.858 }) {
			assertEquals(expectedPageRank, result.get(i++), 0.001);
		}

		mips = 3;
		start = 4;
		size = 3;
		cloudlet.init(start, size, inputMapResults, initialPageRanks, dampingFactor);
		cloudlet.performJob(mips);
		result = cloudlet.getResults();

		assertEquals(size, result.size());
		for (Double expectedPageRank : new Double[] { 0.787, 0.150, 0.150 }) {
			assertEquals(expectedPageRank, result.get(i++), 0.001);
		}
	}

	@Test
	public void testRunWithGreaterMipsTwoCloudlets() {
		int start = 0;
		int size = 4;
		int mips = 40;
		Map<Integer, Double> result;

		cloudlet.init(start, size, inputMapResults, initialPageRanks, dampingFactor);
		cloudlet.performJob(mips);
		result = cloudlet.getResults();
		int i = 0;

		assertEquals(size, result.size());
		for (Double expectedPageRank : new Double[] { 1.212, 1.070, 2.770, 0.858 }) {
			assertEquals(expectedPageRank, result.get(i++), 0.001);
		}

		start = 4;
		size = 3;
		cloudlet.init(start, size, inputMapResults, initialPageRanks, dampingFactor);
		cloudlet.performJob(mips);
		result = cloudlet.getResults();

		assertEquals(size, result.size());
		for (Double expectedPageRank : new Double[] { 0.787, 0.150, 0.150 }) {
			assertEquals(expectedPageRank, result.get(i++), 0.001);
		}
	}

	@Test
	public void testRunWithFewerMipsTwoCloudlets() {
		int start = 0;
		int size = 4;
		int mips = 2;
		Map<Integer, Double> result;

		cloudlet.init(start, size, inputMapResults, initialPageRanks, dampingFactor);
		cloudlet.performJob(mips);
		result = cloudlet.getResults();
		int i = 0;

		assertEquals(mips, result.size());
		for (Double expectedPageRank : new Double[] { 1.212, 1.070 }) {
			assertEquals(expectedPageRank, result.get(i++), 0.001);
		}

		start = 4;
		size = 3;
		cloudlet.init(start, size, inputMapResults, initialPageRanks, dampingFactor);
		cloudlet.performJob(mips);
		result = cloudlet.getResults();

		i = 4;
		assertEquals(mips, result.size());
		for (Double expectedPageRank : new Double[] { 0.787, 0.150 }) {
			assertEquals(expectedPageRank, result.get(i++), 0.001);
		}
	}

	@Test
	public void testRunWithExactMipsThreeCloudlets() {
		int start = 0;
		int size = 3;
		int mips = 3;
		Map<Integer, Double> result;

		cloudlet.init(start, size, inputMapResults, initialPageRanks, dampingFactor);
		cloudlet.performJob(mips);
		result = cloudlet.getResults();
		int i = 0;

		assertEquals(size, result.size());
		for (Double expectedPageRank : new Double[] { 1.212, 1.070, 2.770 }) {
			assertEquals(expectedPageRank, result.get(i++), 0.001);
		}

		mips = 3;
		start = 3;
		size = 3;
		cloudlet.init(start, size, inputMapResults, initialPageRanks, dampingFactor);
		cloudlet.performJob(mips);
		result = cloudlet.getResults();

		assertEquals(size, result.size());
		for (Double expectedPageRank : new Double[] { 0.858, 0.787, 0.150 }) {
			assertEquals(expectedPageRank, result.get(i++), 0.001);
		}

		mips = 1;
		start = 6;
		size = 1;
		cloudlet.init(start, size, inputMapResults, initialPageRanks, dampingFactor);
		cloudlet.performJob(mips);
		result = cloudlet.getResults();

		assertEquals(size, result.size());
		for (Double expectedPageRank : new Double[] { 0.150 }) {
			assertEquals(expectedPageRank, result.get(i++), 0.001);
		}
	}

}