package org.cloudbus.cloudsim.pagerank;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;

import org.cloudbus.cloudsim.UtilizationModel;
import org.cloudbus.cloudsim.UtilizationModelFull;
import org.junit.Test;

public class PageRankUtilsTest {

	@Test
	public void testCombineTwoMapResults() {
		UtilizationModel utilizationModel = new UtilizationModelFull();
		PageRankMapCloudlet cloudlet1 = new PageRankMapCloudlet(0, 400000, 1, 300, 300, utilizationModel,
				utilizationModel, utilizationModel);
		PageRankMapCloudlet cloudlet2 = new PageRankMapCloudlet(1, 400000, 1, 300, 300, utilizationModel,
				utilizationModel, utilizationModel);

		String inputFilePath1 = "src/test/resources/input1";
		String inputFilePath2 = "src/test/resources/input2";
		int mips1 = 7;
		int mips2 = 8;
		Map<Integer, PageRankIntermediateResult> expected = new TreeMap<Integer, PageRankIntermediateResult>();

		cloudlet1.init(inputFilePath1);
		cloudlet2.init(inputFilePath2);

		cloudlet1.performJob(mips1);
		cloudlet2.performJob(mips2);

		expected.put(0, new PageRankIntermediateResult(Arrays.asList(2, 4, 6), 3));
		expected.put(1, new PageRankIntermediateResult(Arrays.asList(0, 2, 6), 1));
		expected.put(2, new PageRankIntermediateResult(Arrays.asList(0, 1, 3, 4, 6), 2));
		expected.put(3, new PageRankIntermediateResult(Arrays.asList(0, 5), 1));
		expected.put(4, new PageRankIntermediateResult(Arrays.asList(5, 6), 2));
		expected.put(5, new PageRankIntermediateResult(Arrays.asList(), 2));
		expected.put(6, new PageRankIntermediateResult(Arrays.asList(), 4));

		assertEquals(expected, PageRankUtils.combine(Arrays.asList(cloudlet1.getResults(), cloudlet2.getResults())));
	}
}
