package org.cloudbus.cloudsim.pagerank.copy;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;

import org.cloudbus.cloudsim.UtilizationModel;
import org.cloudbus.cloudsim.UtilizationModelFull;
import org.cloudbus.cloudsim.pagerank.copy.PageRankCombineCloudlet;
import org.cloudbus.cloudsim.pagerank.copy.PageRankIntermediateResult;
import org.cloudbus.cloudsim.pagerank.copy.PageRankMapCloudlet;
import org.cloudbus.cloudsim.pagerank.copy.PageRankReduceCloudlet;
import org.junit.Before;
import org.junit.Test;

public class PageRankReduceCloudletTest {

	PageRankReduceCloudlet cloudlet;
	Map<String, Double> initialPageRanks;

	@Before
	public void setUp() {
		UtilizationModel utilizationModel = new UtilizationModelFull();

		cloudlet = new PageRankReduceCloudlet(0, 400000, 1, 300, 300, utilizationModel, utilizationModel,
				utilizationModel);

		initialPageRanks = new TreeMap<>();
		initialPageRanks.put("0", 1.0);
		initialPageRanks.put("1", 1.0);
		initialPageRanks.put("2", 1.0);
		initialPageRanks.put("3", 1.0);
	}

	@Test
	public void testValidInputParams() {
		int index = 0;
		int size = 4;
		PageRankCloudlet.setGlobals(4, 7, 1);
		cloudlet.initialize(index);

		assertEquals(index, cloudlet.getIndex());
		assertEquals(size, cloudlet.getSize());
	}

	@Test
	public void testRunWithFullSize() {
		int index = 0;
		PageRankCloudlet.setGlobals(4, 7, 1);
		int mips = 4;
		double dampingFactor = 0.85;

		Map<String, PageRankIntermediateResult> inputs = new TreeMap<String, PageRankIntermediateResult>();
		inputs.put("0", new PageRankIntermediateResult(Arrays.asList("2"), 3));
		inputs.put("1", new PageRankIntermediateResult(Arrays.asList("0", "2"), 1));
		inputs.put("2", new PageRankIntermediateResult(Arrays.asList("0", "1", "3"), 2));
		inputs.put("3", new PageRankIntermediateResult(Arrays.asList("0"), 1));

		Map<String, Double> initialPageRanks = new TreeMap<>();
		initialPageRanks.put("0", 1.0);
		initialPageRanks.put("1", 1.0);
		initialPageRanks.put("2", 1.0);
		initialPageRanks.put("3", 1.0);

		cloudlet.initialize(index, inputs, initialPageRanks, dampingFactor);
		cloudlet.performJob(mips);

		assertEquals(0.575, cloudlet.getResults().get("0"), 0.001);
		assertEquals(0.858, cloudlet.getResults().get("1"), 0.001);
		assertEquals(2.133, cloudlet.getResults().get("2"), 0.001);
		assertEquals(0.433, cloudlet.getResults().get("3"), 0.001);
	}

	@Test
	public void testRunWithFullSizePartialMips() {
		int index = 0;
		PageRankCloudlet.setGlobals(4, 7, 1);
		double dampingFactor = 0.85;
		int mips = 1;

		Map<String, PageRankIntermediateResult> inputs = new TreeMap<String, PageRankIntermediateResult>();
		inputs.put("0", new PageRankIntermediateResult(Arrays.asList("2"), 3));
		inputs.put("1", new PageRankIntermediateResult(Arrays.asList("0", "2"), 1));
		inputs.put("2", new PageRankIntermediateResult(Arrays.asList("0", "1", "3"), 2));
		inputs.put("3", new PageRankIntermediateResult(Arrays.asList("0"), 1));

		Map<String, Double> initialPageRanks = new TreeMap<>();
		initialPageRanks.put("0", 1.0);
		initialPageRanks.put("1", 1.0);
		initialPageRanks.put("2", 1.0);
		initialPageRanks.put("3", 1.0);

		cloudlet.initialize(index, inputs, initialPageRanks, dampingFactor);
		cloudlet.performJob(mips);

		assertEquals(0.575, cloudlet.getResults().get("0"), 0.001);
		assertEquals(null, cloudlet.getResults().get("1"));
		assertEquals(null, cloudlet.getResults().get("2"));
		assertEquals(null, cloudlet.getResults().get("3"));

		cloudlet.performJob(mips);

		assertEquals(0.575, cloudlet.getResults().get("0"), 0.001);
		assertEquals(0.858, cloudlet.getResults().get("1"), 0.001);
		assertEquals(null, cloudlet.getResults().get("2"));
		assertEquals(null, cloudlet.getResults().get("3"));

		cloudlet.performJob(mips);

		assertEquals(0.575, cloudlet.getResults().get("0"), 0.001);
		assertEquals(0.858, cloudlet.getResults().get("1"), 0.001);
		assertEquals(2.133, cloudlet.getResults().get("2"), 0.001);
		assertEquals(null, cloudlet.getResults().get("3"));

		cloudlet.performJob(mips);

		assertEquals(0.575, cloudlet.getResults().get("0"), 0.001);
		assertEquals(0.858, cloudlet.getResults().get("1"), 0.001);
		assertEquals(2.133, cloudlet.getResults().get("2"), 0.001);
		assertEquals(0.433, cloudlet.getResults().get("3"), 0.001);
	}

	@Test
	public void testRunWithAllOperations() {
		UtilizationModel utilizationModel = new UtilizationModelFull();
		String filePath = "src/test/resources/testRunMapCloudlet1";
		int index = 0;
		PageRankCloudlet.setGlobals(4, 7, 1);
		int mips = 7;
		double dampingFactor = 0.85;

		PageRankMapCloudlet mapCloudlet = new PageRankMapCloudlet(0, 400000, 1, 300, 300, utilizationModel,
				utilizationModel, utilizationModel);
		mapCloudlet.initialize(index, filePath);
		mapCloudlet.performJob(mips);

		PageRankCombineCloudlet combineCloudlet = new PageRankCombineCloudlet(0, 400000, 1, 300, 300, utilizationModel,
				utilizationModel, utilizationModel);
		combineCloudlet.initialize(0, Arrays.asList(mapCloudlet.getResults()));
		combineCloudlet.performJob(mips);

		cloudlet.initialize(index, combineCloudlet.getResults(), initialPageRanks, dampingFactor);
		cloudlet.performJob(mips);

		assertEquals(0.575, cloudlet.getResults().get("0"), 0.001);
		assertEquals(0.858, cloudlet.getResults().get("1"), 0.001);
		assertEquals(2.133, cloudlet.getResults().get("2"), 0.001);
		assertEquals(0.433, cloudlet.getResults().get("3"), 0.001);
	}

}
