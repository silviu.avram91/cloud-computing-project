package org.cloudbus.cloudsim.pagerank;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;

import org.cloudbus.cloudsim.UtilizationModel;
import org.cloudbus.cloudsim.UtilizationModelFull;
import org.junit.Before;
import org.junit.Test;

public class PageRankMapCloudletTest {
	PageRankMapCloudlet cloudlet;

	@Before
	public void setUp() {
		UtilizationModel utilizationModel = new UtilizationModelFull();

		cloudlet = new PageRankMapCloudlet(0, 400000, 1, 300, 300, utilizationModel, utilizationModel,
				utilizationModel);
	}

	@Test
	public void testInvalidFileName() {
		try {
			cloudlet.init("src/test/resources/invalidNameForFile");
		} catch (RuntimeException e) {
			assertEquals(e.getMessage(), PageRankUtils.Messages.FILE_NOT_FOUND.getMessage());
			return;
		}

		throw new AssertionError("no message thrown");
	}

	@Test
	public void testValidFileName() {
		String inputFilePath = "src/test/resources/input1";
		try {
			cloudlet.init(inputFilePath);
		} catch (RuntimeException e) {
			throw new AssertionError("error message thrown");
		}

		assertEquals(cloudlet.getInputFileName(), inputFilePath);
	}

	@Test
	public void testRunExactMips() {
		String inputFilePath = "src/test/resources/input1";
		int mips = 7;
		Map<Integer, PageRankIntermediateResult> expected = new TreeMap<Integer, PageRankIntermediateResult>();

		cloudlet.init(inputFilePath);
		cloudlet.performJob(mips);

		expected.put(0, new PageRankIntermediateResult(Arrays.asList(2), 3));
		expected.put(1, new PageRankIntermediateResult(Arrays.asList(0, 2), 1));
		expected.put(2, new PageRankIntermediateResult(Arrays.asList(0, 1, 3), 2));
		expected.put(3, new PageRankIntermediateResult(Arrays.asList(0), 1));

		assertEquals(expected, cloudlet.getResults());
	}

	@Test
	public void testRunMoreMips() {
		String inputFilePath = "src/test/resources/input1";
		int mips = 70;
		Map<Integer, PageRankIntermediateResult> expected = new TreeMap<Integer, PageRankIntermediateResult>();

		cloudlet.init(inputFilePath);
		cloudlet.performJob(mips);

		expected.put(0, new PageRankIntermediateResult(Arrays.asList(2), 3));
		expected.put(1, new PageRankIntermediateResult(Arrays.asList(0, 2), 1));
		expected.put(2, new PageRankIntermediateResult(Arrays.asList(0, 1, 3), 2));
		expected.put(3, new PageRankIntermediateResult(Arrays.asList(0), 1));

		assertEquals(expected, cloudlet.getResults());
	}

	@Test
	public void testRunFewerMips() {
		String inputFilePath = "src/test/resources/input1";
		int mips = 4;
		Map<Integer, PageRankIntermediateResult> expected = new TreeMap<Integer, PageRankIntermediateResult>();

		cloudlet.init(inputFilePath);
		cloudlet.performJob(mips);

		expected.put(0, new PageRankIntermediateResult(Arrays.asList(), 3));
		expected.put(1, new PageRankIntermediateResult(Arrays.asList(0), 1));
		expected.put(2, new PageRankIntermediateResult(Arrays.asList(0, 1), 0));
		expected.put(3, new PageRankIntermediateResult(Arrays.asList(0), 0));

		assertEquals(expected, cloudlet.getResults());
	}

	@Test
	public void testRunFewerMipsButMultipleRuns() {
		String inputFilePath = "src/test/resources/input1";
		int mips = 3;
		Map<Integer, PageRankIntermediateResult> expected = new TreeMap<Integer, PageRankIntermediateResult>();

		cloudlet.init(inputFilePath);
		cloudlet.performJob(mips);

		expected.put(0, new PageRankIntermediateResult(Arrays.asList(), 3));
		expected.put(1, new PageRankIntermediateResult(Arrays.asList(0), 0));
		expected.put(2, new PageRankIntermediateResult(Arrays.asList(0), 0));
		expected.put(3, new PageRankIntermediateResult(Arrays.asList(0), 0));

		assertEquals(expected, cloudlet.getResults());

		cloudlet.performJob(mips);

		expected.clear();
		expected.put(0, new PageRankIntermediateResult(Arrays.asList(2), 3));
		expected.put(1, new PageRankIntermediateResult(Arrays.asList(0, 2), 1));
		expected.put(2, new PageRankIntermediateResult(Arrays.asList(0, 1), 2));
		expected.put(3, new PageRankIntermediateResult(Arrays.asList(0), 0));

		assertEquals(expected, cloudlet.getResults());

		cloudlet.performJob(mips);

		expected.clear();
		expected.put(0, new PageRankIntermediateResult(Arrays.asList(2), 3));
		expected.put(1, new PageRankIntermediateResult(Arrays.asList(0, 2), 1));
		expected.put(2, new PageRankIntermediateResult(Arrays.asList(0, 1, 3), 2));
		expected.put(3, new PageRankIntermediateResult(Arrays.asList(0), 1));

		assertEquals(expected, cloudlet.getResults());
	}

}